# Introduction

SoundCloud's API has been closed for quite some time now, as they have closed their Google Form for applications for new API users. This polished actor is here to fill this gap in the market.

### Table of contents

<!-- toc start -->

-   [Features](#what-can-it-do)
-   [Use Cases](#use-cases)
-   [Input](#input)
-   [Output](#output)
    <!-- - Miscellaneous -->
    <!-- toc end -->

## What Can it Do?

Based on your inputs, you can receive all of the following with a username or query:

-   User data
-   Track URLs & data
-   Track embed HTML data
-   Track comments data

When provided a list of usernames/queries/URLs, this actor will simultaneously scrape them, and push them all to the dataset, separated into objects titled after the input provided.

## Use Cases

1. ### Mass embedding user tracks onto a site's page

2. ### Collecting statistics about a user based on their metrics on the SoundCloud platform

3. ### Collecting more social media information about a SoundCloud user

4. ### Gathering data on which tracks are most popular on SoundCloud based on specific queries

5. ### Integrating SoundCloud search results into your app

#### This scraper will fit many more use cases as development continues

## Input

| Field             | Type    | Description                                                                                                                                                             | Default |
| ----------------- | ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | - |
| usernames         | array   | List of users to scrape.                                                                                                                                                | [] |
| keywords          | array   | Queries to search SoundCloud and scrape by.                                                                                                                             | [] |
| URLs              | array   | A list of URLs to scrape tracks from.                                                                                                                                   | [] |
| wantFullTrackData | boolean | Defines whether or not you want just track URLs, or want full data for each track.                                                                     | false |
| maxComments       | number  | The maximum number of comments you want to scrape from each track.                                                                                                      | 0 |
| wantEmbed         | boolean | Defines whether or not to fetch the embed HTML of every single track before adding it to the dataset (If true, scrape will take significantly longer). | false |

### Example Input

```JSON
{
  "usernames": ["diplo", "mestomusic"],
  "wantFullTrackData": true,
  "maxComments": 50,
  "wantEmbed": false
}
```

## Output

### User Info

Example of partial output from an input with "type" set to 'USER' and "keyword" set to 'kodak-black':

```JSON
{
  "id": 72181005,
  "url": "https://soundcloud.com/kodak-black",
  "username": "kodak-black",
  "name": "Kodak Black",
  "profilePicture": "https://i1.sndcdn.com/avatars-8451osYsrs6ct2rR-OFcMAw-large.jpg",
  "profileBanner": "https://i1.sndcdn.com/visuals-000072181005-bJh6nS-original.jpg",
  "location": null,
  "countryCode": null,
  "verified": true,
  "proUnlimited": false,
  "description": "\"Before I Go (feat. Rod Wave)\" Out Now!\nStream/Download: https://Kodak.lnk.to/BeforeIGo",
  "creationDate": "2013-12-27T17:04:36.000Z",
  "lastModified": "2021-10-27T00:00:01.000Z",
  "followersCount": 1552116,
  "followingCount": 2,
  "likesCount": 17,
  "tracksCount": 296,
  "playlistCount": 20,
  "socials": [
    {
      "name": "Instagram",
      "link": "https://gate.sc?url=http%3A%2F%2Finstagram.com%2FKodakBlack&token=99586f-1-1641820864331"
    },
    {
      "name": "Facebook",
      "link": "https://gate.sc?url=https%3A%2F%2Fwww.facebook.com%2FTheRealKodakBlack%2F&token=f183b-1-1641820864332"
    },
    {
      "name": "Twitter",
      "link": "https://gate.sc?url=https%3A%2F%2Ftwitter.com%2FKodakBlack1k&token=b4db3e-1-1641820864333"
    },
    {
      "name": "Website",
      "link": "https://gate.sc?url=http%3A%2F%2Fofficialkodakblack.com%2F&token=96498b-1-1641820864333"
    },
    {
      "name": "Youtube",
      "link": "https://gate.sc?url=https%3A%2F%2Fwww.youtube.com%2Fc%2FKodakBlack&token=c39ef-1-1641820864334"
    }
  ]
}
```

Example track output:

```JSON
{
  "id": 1178009647,
  "title": "Love & War",
  "description": null,
  "genre": "Rap/Hip Hop",
  "thumbnail": "https://i1.sndcdn.com/artworks-dSqKt7t4vZ5D-0-large.jpg",
  "tags": [
    ""
  ],
  "url": "https://soundcloud.com/kodak-black/love-war",
  "purchaseData": {
    "title": null,
    "url": null
  },
  "downloadable": false,
  "publishedAt": "2021-12-15T00:18:35.000Z",
  "commentCount": 179,
  "likeCount": 11447,
  "repostCount": 244,
  "embedHTML": "https://soundcloud.com/oembed?url=https%3A%2F%2Fsoundcloud.com%2Fkodak-black%2Flove-war&format=json",
  "comments": []
}
```

Example comments array within tracks output:

```JSON
[
  {
    "userLink": "https://soundcloud.com//ion_music",
    "name": ".ION.",
    "body": "sheeeesh"
  },
  {
    "userLink": "https://soundcloud.com//k_dubs",
    "name": "𝕶𝖚𝖍𝖑𝖔𝖘𝖚𝖑",
    "body": "@ion_music sheeeeesh"
  },
  {
    "userLink": "https://soundcloud.com//handrointhecut",
    "name": "HANDRO",
    "body": "You killed this"
  }
]
```
