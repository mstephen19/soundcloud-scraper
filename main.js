const Apify = require('apify');
const ScClient = require('./src/scClient');

const { log, puppeteer } = Apify.utils;

Apify.main(async () => {
    // await Apify.setValue('INPUT', {
    //     keywords: [],
    //     usernames: ['k_dubs'],
    //     urls: [],
    //     wantFullTrackData: true,
    //     maxComments: 200,
    //     wantEmbed: false,
    // });

    const input = await Apify.getInput();
    const soundcloud = new ScClient(input);

    const requestList = await soundcloud.createRequestList();
    const requestQueue = await Apify.openRequestQueue();

    const crawler = new Apify.PuppeteerCrawler({
        requestList,
        requestQueue,
        handlePageTimeoutSecs: 240,
        minConcurrency: 5,
        maxConcurrency: 50,
        launchContext: {
            launchOptions: {
                headless: true,
            },
        },
        handlePageFunction: async (context) => {
            const { label } = context.request.userData;
            await puppeteer.blockRequests(context.page, {
                urlPatterns: ['.css', '.jpg', '.jpeg', '.png', '.svg', '.gif', '.woff', '.pdf', '.zip'],
            });

            switch (label) {
                default: {
                    log.error('Unknown request found in the queue!');
                    throw new Error(`Something's gone wrong...`);
                }
                case 'USER': {
                    return soundcloud.getUserData(context);
                }
                case 'QUERY': {
                    return soundcloud.tracksByQuery(context);
                }
                case 'URL': {
                    return soundcloud.tracksByQuery(context);
                }
                case 'QUERY_TRACK': {
                    return soundcloud.getTrackData(context, label);
                }
                case 'USER_TRACK': {
                    return soundcloud.getTrackData(context, label);
                }
            }
        },
    });

    log.info('Starting the crawl.');
    await crawler.run();
    await soundcloud.compileData();
    log.info('Crawl finished.');
});
