const { log } = require('apify').utils;

module.exports = {
    /**
     *
     * @param {object} data Parsed from HTML on user page
     * @returns {object}
     */
    createUserObject(data) {
        // Take in our data object and transform it into our own format
        return {
            id: data?.id,
            url: data?.permalink_url,
            username: data?.permalink,
            name: data?.username,
            profilePicture: data?.avatar_url,
            profileBanner: data?.visuals?.visuals[0].visual_url,
            location: data?.city,
            countryCode: data?.country_code,
            verified: data?.verified,
            proUnlimited: data?.badges.pro_unlimited,
            description: data?.description,
            creationDate: new Date(data?.created_at),
            lastModified: new Date(data?.last_modified),
            followersCount: data?.followers_count,
            followingCount: data?.followings_count,
            likesCount: data?.likes_count,
            tracksCount: data?.track_count,
            playlistCount: data?.playlist_count,
        };
    },
    /**
     *
     * @param {object} data Parsed from HTML on track page
     * @returns {object}
     */
    createTrackObject(data) {
        return {
            id: data?.id,
            title: data?.title,
            username: data?.user?.permalink,
            description: data?.description,
            genre: data?.genre,
            thumbnail: data?.artwork_url,
            tags: data?.tag_list.split(' '),
            url: data?.permalink_url,
            purchaseData: {
                title: data?.purchase_title,
                url: data?.purchase_url,
            },
            downloadable: data?.downloadable,
            publishedAt: new Date(data?.created_at),
            commentCount: data?.comment_count,
            likeCount: data?.likes_count,
            repostCount: data?.reposts_count,
        };
    },
    /**
     *
     * @param {object} input Input from the user
     * @returns
     */
    validateInput({ urls = [], usernames = [], keywords = [], wantFullTrackData, wantEmbed, maxComments }) {
        // Make sure each keyword is a string
        for (const keyword of keywords) {
            if (typeof keyword !== 'string') throw new Error('Keyword must be a string!');
        }

        // If one of the usernames includes a space, throw an error
        for (const username of usernames) {
            if (username.match(/\s/g)) throw new Error("SoundCloud usernames can't have spaces!");
        }

        // Check if each URL is in fact a valid URL
        for (const url of urls) {
            const works = (async () => {
                try {
                    return new URL(url) || new URL(`https://${url}`);
                } catch (err) {
                    return err;
                }
            })();
            if (!works || !url.includes('soundcloud.com')) throw new Error('Invalid SoundCloud URL provided!');
            if (!url.startsWith('https://')) log.warning('Please include "https://" at the beginning of the URL next time.');
        }

        if (typeof wantFullTrackData !== 'boolean' || typeof wantEmbed !== 'boolean') {
            throw new Error('"want" values must be booleans!');
        }

        if (maxComments < 0) throw new Error('MaxComments value can only be 0 to 200!');

        // Who wants more than 200 comments per track anyways?
        if (maxComments > 200) throw new Error('MaxComments must be 200 or less! (For dataset issues)');
    },
    /**
     *
     * @param {array} tracks Array of tracks scraped and soon to be added to the requestQueue
     * @param {string} term Username/URL/Query that produced the tracks array
     * @returns
     */
    timeEstimateLog(tracks, term) {
        // This generates a very rough estimate of how long it will take to go through a list of track requests
        const estimate = Math.ceil(tracks.length / 18);

        if (tracks.length > 100) {
            return log.warning(`Found ${tracks.length} tracks for ${term}. This operation may take up to ${estimate} minute(s).`);
        }
        log.info(`Found ${tracks.length} tracks for ${term}. This operation may take up to ${estimate} minute(s).`);
    },
};
