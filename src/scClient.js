const Apify = require('apify');
const { log, puppeteer } = Apify.utils;

const axios = require('axios');

const { BASE_URL } = require('./constants');
const { createUserObject, createTrackObject, validateInput, timeEstimateLog } = require('./helpers');

class ScClient {
    constructor(input) {
        validateInput(input);

        this.usernames = input.usernames || [];
        this.keywords = input.keywords || [];
        this.urls = input.urls || [];
        this.wantFullTrackData = input.wantFullTrackData;
        this.maxComments = input.maxComments;
        this.wantEmbed = input.wantEmbed;
        this.users = {};
        this.queries = {};
    }

    /**
     *
     * @returns {Promise} openRequestList
     */
    async createRequestList() {
        const requests = [];

        // Create a request for every single keyword, username, and URL provided.
        for (const keyword of this.keywords) {
            requests.push({ url: `${BASE_URL}/search/sounds?q=${keyword}`, userData: { label: 'QUERY', query: keyword } });
        }

        for (const username of this.usernames) {
            requests.push({ url: `${BASE_URL}/${username}/tracks`, userData: { label: 'USER' } });
        }

        for (const url of this.urls) {
            if (!url.startsWith('https://') && !url.startsWith('http://')) {
                requests.push({ url: `https://${url}`, userData: { label: 'URL', query: url } });
            } else {
                requests.push({ url, userData: { label: 'URL', query: url } });
            }
        }

        log.info('Finished handling user input.');
        return Apify.openRequestList('start-urls', [...requests]);
    }

    /**
     *
     * @param {object} context Context from the crawler
     * @returns
     */
    async getUserData({ page, crawler: { requestQueue } }) {
        await page.waitForSelector('#content', { timeout: 2500 });

        const html = await page.evaluate(() => document.querySelector('html').innerHTML);
        const { data } = JSON.parse(html.split('window.__sc_hydration =')[1].split('</script>')[0].replace(';', ''))[5];

        // Create basic user data object
        const user = {
            ...createUserObject(data),
            socials: await page.$$eval('a.web-profile', ($els) => {
                const arr = [];
                $els.forEach((a) => {
                    arr.push({
                        name: a.innerText.trim(),
                        link: a.getAttribute('href').includes('mailto:') ? a.getAttribute('href').split('mailto:')[1] : a.getAttribute('href'),
                    });
                });
                return arr;
            }),
        };
        log.info(`Scraped basic info for: ${user.username}`);

        await puppeteer.infiniteScroll(page, { waitForSecs: 2 });

        // Grab all of the user's tracks links
        user.tracks = await this.scrapeTrackLinks(page);
        log.info(`Scraped track links for: ${user.username}`);

        // If this is all that was wanted, return out and push to dataset
        if (!this.wantFullTrackData) return Apify.pushData({ [user.username]: user });

        timeEstimateLog(user.tracks, user.username);

        // Otherwise, add a request to the queue for each track
        for (const url of user.tracks) {
            await requestQueue.addRequest({ url: url, userData: { label: 'USER_TRACK' } });
        }

        user.tracks = [];

        this.users[user.username] = user;
    }

    /**
     *
     * @param {object} context Context from the crawler
     * @param {string} label Label of the request (Pulled from the context)
     * @returns
     */
    async getTrackData({ page, request }, label) {
        await page.waitForSelector('#content', { timeout: 4000 });

        // Handle embed HTML output based on input
        const embedHTMLUrl = await page.$eval('link[type="text/json+oembed"]', ($el) => $el.getAttribute('href'));
        const embedHTML = this.wantEmbed
            ? await axios({
                  method: 'get',
                  url: embedHTMLUrl,
              }).then(({ data }) => data.html.replace('\\', ''))
            : embedHTMLUrl;

        const commentsCount = (await page.$('.commentsList__actualTitle'))
            ? await page.$eval('.commentsList__actualTitle', ($el) => parseInt($el?.innerText.split(' ')[0], 10))
            : 0;

        // Handle comments output based on input
        const comments = (await this.scrapeComments(page, commentsCount)) || [];

        const html = await page.evaluate(() => document.querySelector('html').innerHTML);
        const str = html.split('window.__sc_hydration =')[1].split('</script>')[0];
        const { data } = JSON.parse(str.slice(0, str.length - 1))[6];

        // Create the track object
        const track = {
            ...createTrackObject(data),
            embedHTML,
            commentsCount,
            comments,
        };
        log.info(`Scraped track: ${track.title}`);

        const query = request.userData?.query;

        // Push our data to our class state objects for later use
        if (label.includes('USER')) this.users[track.username].tracks = [...this.users[track.username]?.tracks, track];
        if (label.includes('QUERY')) {
            if (!this.queries[query]) this.queries[query] = [];
            this.queries[query] = [...this.queries[query], track];
        }
    }

    /**
     *
     * @param {object} context Context from the crawler
     * @returns
     */
    async tracksByQuery({ page, request, crawler: { requestQueue } }) {
        const { query } = request.userData;
        await puppeteer.infiniteScroll(page, { waitForSecs: 3 });
        const tracks = await this.scrapeTrackLinks(page);
        if (!this.wantFullTrackData) return Apify.pushData({ trackList: [...tracks] });

        timeEstimateLog(tracks, query);

        for (const url of tracks) {
            await requestQueue.addRequest({ url: url, userData: { label: 'QUERY_TRACK', query } });
        }
    }

    /**
     *
     * @param {object} page Puppeteer page object
     * @returns
     */
    async scrapeTrackLinks(page) {
        return page.$$eval('a.sound__coverArt', ($els) => {
            const arr = [];
            $els.forEach((a) => {
                const link = a.getAttribute('href');
                arr.push(`https://soundcloud.com/${link}`);
            });
            return arr;
        });
    }

    /**
     *
     * @param {object} page
     * @param {number} commentsCount
     * @returns {Promise} comments
     */
    async scrapeComments(page, commentsCount) {
        if (this.maxComments !== 0 && commentsCount !== 0) {
            let comments = [];
            // Create a count variable to track our comments count and determine to keep scrolling
            let count = 0;
            let attempts = 0;
            // If the comments on the track is none, just skip over the scrolling logic
            if (commentsCount === 0) {
                count = this.maxComments;
                return comments;
            }

            while (count < this.maxComments) {
                // If comments on track is less than our max number, scroll to bottom and scrape all comments
                if (commentsCount <= this.maxComments) {
                    await puppeteer.infiniteScroll(page, { waitForSecs: 2 });
                    count = this.maxComments;
                } else {
                    // Scroll a little bit down, then update count with how many comments are currently loaded on the page
                    await puppeteer.infiniteScroll(page, { waitForSecs: 1, timeoutSecs: 3 });
                    count = await page.$$eval('.commentsList__item', ($els) => Array.from($els).length);

                    // Sometimes, commentsCount does not accurately represent the comments items under the track. In this case, track attempts
                    attempts += 1;
                    if (attempts > 25) count = this.maxComments;
                }
            }

            if (attempts > 25)
                log.warning(
                    `There was an issue with one of the tracks.\nSoundCloud doesn't always accurately display comment count.\nThis has been compensated for and automatically handled.`
                );

            // Scrape comments
            comments = await page.$$eval(
                '.commentsList__item',
                ($els, maxComments) => {
                    const arr = [];

                    for (const li of $els) {
                        if (arr.length >= maxComments) return arr;
                        arr.push({
                            userLink: `https://soundcloud.com/${li.querySelector('.commentItem__avatar').getAttribute('href')}`,
                            name: li.querySelector('.commentItem__username > a').innerText,
                            body: li.querySelector('.commentItem__body > span > p').innerText,
                        });
                    }

                    return arr;
                },
                this.maxComments
            );

            return comments;
        }
    }

    async compileData() {
        log.info('Compiling and pushing data to the dataset. Please be patient...');
        for (const user of Object.values(this.users)) {
            await Apify.pushData({ [user.username]: user });
        }

        for (const trackList of Object.values(this.queries)) {
            await Apify.pushData({ trackList });
        }
    }
}

module.exports = ScClient;
